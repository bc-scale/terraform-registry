package utils

import (
	"fmt"
	"strings"
)

type ProviderPackage struct {
	Name    string
	Version string
	OS      string
	Arch    string
}

func (p *ProviderPackage) AsFilename() string {
	return fmt.Sprintf("%s_%s_%s_%s.zip", p.Name, p.Version, p.OS, p.Arch)
}

func ParseProviderPackage(name string) (*ProviderPackage, error) {
	// split the filename and remove the extension
	parts := strings.Split(strings.TrimRight(name, ".zip"), "_")
	if len(parts) != 4 {
		return nil, fmt.Errorf("expected length of 4, got %d", len(parts))
	}
	return &ProviderPackage{
		Name:    parts[0],
		Version: parts[1],
		OS:      parts[2],
		Arch:    parts[3],
	}, nil
}
