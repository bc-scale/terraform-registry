package dao

import (
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/utils"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type DatabaseManager struct {
	db *gorm.DB
}

func NewDatabaseManager(path string) (*DatabaseManager, error) {
	db, err := gorm.Open(sqlite.Open(path), &gorm.Config{
		Logger: utils.NewGormLogger(),
	})
	if err != nil {
		log.WithError(err).Error("failed to open database connection")
		return nil, err
	}

	mgr := new(DatabaseManager)
	mgr.db = db

	if err = mgr.init(); err != nil {
		log.WithError(err).Error("failed to initialise database")
		return nil, err
	}

	return mgr, nil
}

func (m *DatabaseManager) init() error {
	return m.db.AutoMigrate(&ProviderRecord{})
}

func (m *DatabaseManager) AddProvider(provider *ProviderRecord) (*ProviderRecord, error) {
	existing, err := m.GetProviderByFilename(provider.Filename)
	if err != nil {
		log.WithError(err).Info("failed to find existing provider, creating...")
		existing = new(ProviderRecord)
	}

	existing.Namespace = provider.Namespace
	existing.Type = provider.Type
	existing.Version = provider.Version
	existing.OS = provider.OS
	existing.Arch = provider.Arch
	existing.Name = provider.Name
	existing.Filename = provider.Filename
	existing.ShaSum = provider.ShaSum
	existing.RegistryPath = provider.RegistryPath

	tx := m.db.Save(existing)
	return existing, tx.Error
}

func (m *DatabaseManager) FindProvider(namespace, pType, version, os, arch string) (*ProviderRecord, error) {
	var record ProviderRecord
	tx := m.db.Where("Namespace = ? AND Type = ? AND Version = ? AND OS = ? AND Arch = ?", namespace, pType, version, os, arch).First(&record)
	return &record, tx.Error
}

func (m *DatabaseManager) GetProviderByFilename(filename string) (*ProviderRecord, error) {
	var record ProviderRecord
	tx := m.db.Where("Filename = ?", filename).First(&record)
	return &record, tx.Error
}

func (m *DatabaseManager) ListVersions(namespace, pType string) ([]ProviderRecord, error) {
	var versions []ProviderRecord
	tx := m.db.Where("Namespace = ? AND Type = ?", namespace, pType).Find(&versions)
	return versions, tx.Error
}
