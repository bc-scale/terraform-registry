package registry

import (
	"context"
	"errors"
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/hashicorp/terraform/registry/response"
	"github.com/levigross/grequests"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type ProxyRegistry struct {
	url    string
	client *http.Client
}

func NewProxyRegistry(url string) *ProxyRegistry {
	r := new(ProxyRegistry)
	r.url = url
	r.client = &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
		},
	}
	tracer.Apply(r.client)

	log.Infof("building proxy registry with target '%s'", url)

	return r
}

func (r *ProxyRegistry) ListAvailableVersions(ctx context.Context, namespace, pType string) (*response.TerraformProviderVersions, int, error) {
	requestId := tracer.GetContextId(ctx)
	target := fmt.Sprintf("%s/v1/providers/%s/%s/versions", r.url, namespace, pType)
	log.Debugf("attempting to list versions at target: %s", target)
	resp, err := grequests.Get(target, &grequests.RequestOptions{})
	// error handling
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to execute proxied ListAvailableVersions request")
		return nil, http.StatusInternalServerError, err
	}
	log.WithField("id", requestId).Infof("request was completed with response code: %d", resp.StatusCode)
	if !resp.Ok {
		log.WithField("id", requestId).Errorf("received unexpected response code (%d) from ListAvailableVersions", resp.StatusCode)
		return nil, resp.StatusCode, errors.New("unexpected response code")
	}
	// convert the response back from json
	var versions response.TerraformProviderVersions
	err = resp.JSON(&versions)
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to unmarshal json response")
		return nil, http.StatusInternalServerError, err
	}
	return &versions, resp.StatusCode, nil
}

func (r *ProxyRegistry) FindPackage(ctx context.Context, namespace, pType, version, os, arch string) (*response.TerraformProviderPlatformLocation, int, error) {
	requestId := tracer.GetContextId(ctx)
	target := fmt.Sprintf("%s/v1/providers/%s/%s/%s/download/%s/%s", r.url, namespace, pType, version, os, arch)
	log.Debugf("attempting to locate package at target: %s", target)
	resp, err := grequests.Get(target, &grequests.RequestOptions{})
	// error handling
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to execute proxied FindPackage request")
		return nil, http.StatusInternalServerError, err
	}
	log.WithField("id", requestId).Infof("request was completed with response code: %d", resp.StatusCode)
	if !resp.Ok {
		log.WithField("id", requestId).Errorf("received unexpected response code (%d) from FindPackage", resp.StatusCode)
		return nil, resp.StatusCode, errors.New("unexpected response code")
	}
	// convert the response back from json
	var pkg response.TerraformProviderPlatformLocation
	err = resp.JSON(&pkg)
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to marshal json response")
		return nil, http.StatusInternalServerError, err
	}
	return &pkg, resp.StatusCode, nil
}

func (r *ProxyRegistry) PublishPackage(_ context.Context, _, _, _ string, _ *PublishOpts) error {
	return errors.New("not implemented")
}
