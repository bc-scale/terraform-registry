terraform {
  required_providers {
    helm = {
      source = "localhost:8080/hashicorp/helm"
      version = "1.3.2"
    }
  }
}