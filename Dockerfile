FROM registry.gitlab.com/av1o/base-images/go-git

RUN mkdir -p /home/somebody/go && \
    mkdir -p /home/somebody/.tmp
WORKDIR /home/somebody/go

ARG GOPRIVATE

ARG AUTO_DEVOPS_GO_GIT_CFG
ARG AUTO_DEVOPS_GO_COMPILE_FLAGS
ARG AUTO_DEVOPS_GO_COMPILE_ENV

# copy our code in
COPY --chown=somebody:0 . .

# KANIKO_BUILD_ARGS=AUTO_DEVOPS_GO_GIT_CFG="url.git@github.com.insteadOf https://github.com"
RUN if [[ -n "${AUTO_DEVOPS_GO_GIT_CFG}" ]]; then git config --global $(echo "${AUTO_DEVOPS_GO_GIT_CFG}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g'); fi

# build the binary
RUN if [ -d "./cmd" ]; then BUILD_DIR="./cmd/..."; else BUILD_DIR="."; fi && \
	TMPDIR=/home/somebody/.tmp CGO_ENABLED=1 GOOS=linux GOARCH=amd64 $(echo "${AUTO_DEVOPS_GO_COMPILE_ENV}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') go build -a -installsuffix cgo -ldflags '-extldflags "-static"' $(echo "${AUTO_DEVOPS_GO_COMPILE_FLAGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') -o main "${BUILD_DIR}"

# runner
FROM registry.gitlab.com/av1o/base-images/scratch

WORKDIR /app
# copy the compiled binary from the builder
COPY --from=0 /home/somebody/go/main /app/
COPY --from=0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

USER somebody

EXPOSE 8080
ENTRYPOINT ["/app/main"]

